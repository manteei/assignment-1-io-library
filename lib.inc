section .text

%define EXIT_CODE 60
%define STDOUT 1
%define SYS_WRITE 0

 	 
;Принимает код возврата и завершает текущий процесс
;Параметры:
;rdi: код возврата
;Вывод:
; !exit
exit:
 	mov rax, EXIT_CODE
 	syscall
 	 
;Принимает указатель на нуль-терминированную строку, возвращает её длину
;Параметры:
;rdi: указатель на нуль-терминированную строку
;Вывод:
;rax: длина строки
string_length:
 	xor rax, rax
 	.loop:
	 	cmp byte[rdi + rax], 0
	 	jne .next
	 	ret
	 	.next:
	 	inc rax
	 	jmp .loop
;Принимает указатель на нуль-терминированную строку, выводит её в stdout
;Параметры:
;rdi: указатель на нуль-терминированную строку
;Вывод:
;!stdout
print_string:
 	call string_length
 	mov rsi, rdi
 	mov rdx, rax
 	mov rdi, STDOUT
 	mov rax, STDOUT
 	syscall
 	mov rax, SYS_WRITE
 	ret
 	 
;Переводит строку (выводит символ с кодом 0xA)
;Параметры:
;-
;Вывод:
;!stdout
print_newline:
 	mov rdi, 0xA
 	 
;Принимает код символа и выводит его в stdout
;Параметры:
;rdi: код символа
;Вывод:
;!stdout
print_char:
 	mov rax, STDOUT
 	mov rdx, STDOUT
 	push rdi
 	mov rdi, STDOUT 
 	mov rsi, rsp
 	syscall
 	pop rdi
 	ret
 	 
;Выводит знаковое 8-байтовое число в десятичном формате 
;Параметры:
;rdi: знаковое 8-байтовое число в десятичном формате
;Вывод:
;!stdout
print_int:
 	cmp rdi, SYS_WRITE
 	jge print_uint
 	push rdi
 	mov rdi, '-' 
 	call print_char 
 	pop rdi 
 	neg rdi 
 	 
;Выводит беззнаковое 8-байтовое число в десятичном формате 
;Совет: выделите место в стеке и храните там результаты деления
;Не забудьте перевести цифры в их ASCII коды.
;Параметры:
;rdi: беззнаковое 8-байтовое число в десятичном формате
;Вывод:
;!stdout
print_uint:
 	mov r9, rsp 
 	mov r10, 10 
 	mov rax, rdi 
 	push SYS_WRITE 
 	.loop:
 	mov rdx, SYS_WRITE 
 	div r10 
 	add rdx, 0x30 
 	dec rsp 
 	mov byte[rsp], dl 
 	cmp rax, SYS_WRITE 
 	jnz .loop
 	 
 	mov rdi, rsp
 	push r9  
	call print_string 
 	pop r9 
 	mov rsp, r9 
 	ret
 	 
;Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
;Параметры:
;rdi: указатель на первую нуль-терминированную строку
;rsi: указатель на вторую нуль-терминированную строку
;Вывод:
;rax: результат
string_equals:
 	mov rax, SYS_WRITE 
 	call string_length 
 	mov r10, rax 
 	push rdi 
 	mov rdi, rsi 
 	pop rsi 
 	call string_length 
 	cmp rax, r10 
 	jne .not_equals
 	.loop:
 	mov rax, SYS_WRITE 
 	mov al, byte[rsi] 
 	cmp al, byte[rdi] 
 	jne .not_equals 
 	cmp al, SYS_WRITE
 	je .equals
 	inc rdi 
 	inc rsi 
 	jmp .loop 
 	.not_equals:
 	mov rax, SYS_WRITE 
 	ret
 	.equals: 
 	mov rax, STDOUT 
 	ret 
 	 
;Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
;Параметры:
;!stdin
;Вывод:
;rax: введенный символ
read_char:
 	push SYS_WRITE 
 	mov rax, SYS_WRITE 
 	mov rdx, STDOUT 
 	mov rdi, SYS_WRITE 
 	mov rsi, rsp 
 	syscall
 	pop rax 
 	ret 
 	 
;Принимает: адрес начала буфера, размер буфера
;Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
;Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
;Останавливается и возвращает 0 если слово слишком большое для буфера
;При успехе возвращает адрес буфера в rax, длину слова в rdx.
;При неудаче возвращает 0 в rax
;Эта функция должна дописывать к слову нуль-терминатор
;Параметры:
;rdi: адрес начала буфера
;rsi: размер буфера
;!stdin
;Вывод:
;.при успехе:
;rax: адрес буфера
;rdx: длина слова
;.при неудаче:
;rax: 0
read_word:
 	push r12
 	push r13
 	push r14 
 	mov r12, rdi 
 	mov r13, rsi 
 	dec r13 
 	mov r14, SYS_WRITE 
 	.check_first:
	 	call read_char 
	 	cmp rax, 0x0 
	 	je .exit 
	 	cmp rax, 0x20 
	 	je .check_first 
	 	cmp rax, 0x9 
	 	je .check_first 
	 	cmp rax, 0xA 
	 	je .check_first 
 	.loop:
	 	cmp rax, 0x0 
	 	je .exit 
	 	cmp rax, 0x20 
	 	je .exit 
	 	cmp rax, 0x9 
	 	je .exit 
	 	cmp rax, 0xA 
	 	je .exit 
	 	mov [r12+r14], al 
	 	inc r14 
	 	cmp r14, r13 
	 	jg .fail 
	 	call read_char 
	 	jmp .loop 
 	.fail:
	 	pop r14
	 	pop r13
	 	pop r12
	 	mov rax, SYS_WRITE
	 	ret
 	.exit:
	 	mov byte[r12+r14], SYS_WRITE 
	 	mov rax, r12 
	 	mov rdx, r14 
	 	pop r14 
	 	pop r13
	 	pop r12 
	 	ret
 	 
;Принимает указатель на строку, пытается
;прочитать из её начала беззнаковое число.
;Возвращает в rax: число, rdx : его длину в символах
;rdx = 0 если число прочитать не удалось
;Параметры:
;rdi: указатель на строку
;Вывод:
;.если удалось прочитать:
;rax: число
;rdx: длина числа в символах
;.если не удалось прочитать:
;rdx: 0;
parse_uint:
 	mov rax, SYS_WRITE 
 	mov r10, 10 
 	mov r9, SYS_WRITE 
 	.loop:
	 	mov r8, SYS_WRITE 
	 	mov r8b, byte[rdi + r9] 
	 	cmp r8b, 0x30 
	 	jb .exit 
	 	cmp r8b, 0x39 
	 	ja .exit 
	 	sub r8b, 0x30 
	 	mul r10 
	 	add rax, r8 
	 	inc r9 
	 	jmp .loop 
 	.exit:
 		mov rdx, r9 
 	ret
 	 
 	 
;Принимает указатель на строку, пытается
;прочитать из её начала знаковое число.
;Если есть знак, пробелы между ним и числом не разрешены.
;Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
;rdx = 0 если число прочитать не удалось
;Параметры:
;rdi: указатель на строку
;Вывод:
;.если удалось прочитать:
;rax: число
;rdx: длина числа в символах
;.если не удалось прочитать:
;rdx: 0;
parse_int:
 	cmp byte[rdi], '-'
 	jne parse_uint 
 	inc rdi
	call parse_uint
 	inc rdx 
 	neg rax 
 	ret
 	 
;Принимает указатель на строку, указатель на буфер и длину буфера
;Копирует строку в буфер
;Возвращает длину строки если она умещается в буфер, иначе 0
;Параметры:
;rdi: указатель на строку
;rsi: указатель на буфер
;rdx: длина буфера
;Вывод:
;rax: длина строки
;rax: 0 - если строка не умещается в буфер
string_copy:
 	push rdi
 	push rsi
 	push rdx 
 	call string_length 
 	inc rax 
 	pop rdx
 	pop rsi
 	pop rdi 
 	cmp rax, rdx 
 	jg .fail 
 	.loop:
	 	mov r8, SYS_WRITE 
	 	mov r8b, byte[rdi] 
	 	mov byte[rsi], r8b 
	 	inc rdi 
	 	inc rsi 
	 	cmp r8b, SYS_WRITE 
	 	jne .loop 
 	ret
 	.fail:
	 	mov rax, SYS_WRITE ;Кладем 0 в rax
	 	ret
